"""
This is part of the MSS Python's module.
Source: https://github.com/BoboTiG/python-mss
OpenCV/Numpy example.
"""

import time
import cv2
import mss
import numpy
import torch
import glob
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--record', type=bool)
parser.add_argument('--top', type=int)
parser.add_argument('--left', type=int)
args = parser.parse_args()


# Model
model = torch.hub.load('ultralytics/yolov5', 'yolov5s', pretrained=True)


frametimes = []

do_record = args.record
filename = f"{time.time()}.mp4"

pad_top = args.top
pad_left = args.left
frameSize = (1920-2*pad_left, 1080-2*pad_top)

if do_record:
    out = cv2.VideoWriter('filename',cv2.VideoWriter_fourcc(*'MP4V'), 15, frameSize)
    with mss.mss() as sct:
        # Part of the screen to capture
        # monitor = {"top": 200, "left": 100, "width": 1920-200, "height": 1080-400}

        monitor_number = 1
        mon = sct.monitors[monitor_number]

        print(sct.monitors)

        monitor = {
                "top": mon["top"] + pad_top,  # 100px from the top
                "left": mon["left"] + pad_left,  # 100px from the left
                "width": 1920-2*pad_left,
                "height": 1080-2*pad_top,
                "mon": monitor_number,
            }

        while "Screen capturing":
            last_time = time.time()

            # Get raw pixels from the screen, save it to a Numpy array
            img = numpy.array(sct.grab(monitor))
            results = model(img)

            # Display the picture
            # 
            results.render()
            cv2.imshow("OpenCV/Numpy normal", results.imgs[0])
            out.write(results.imgs[0])
            #results.

            # Display the picture in grayscale
            # cv2.imshow('OpenCV/Numpy grayscale',
            #            cv2.cvtColor(img, cv2.COLOR_BGRA2GRAY))

            #results = model(img)
            t = 1 / (time.time() - last_time)
            print("\nfps: {}".format(t))
            frametimes.append(t)
            print(f"{sum(frametimes)/len(frametimes)}")
            #cv2.imshow("OpenCV/Numpy normal", img)

            # Press "q" to quit
            if cv2.waitKey(25) & 0xFF == ord("q"):
                cv2.destroyAllWindows()
                break
            
        out.release()
else:
    with mss.mss() as sct:
        # Part of the screen to capture
        # monitor = {"top": 200, "left": 100, "width": 1920-200, "height": 1080-400}

        monitor_number = 1
        mon = sct.monitors[monitor_number]

        print(sct.monitors)

        monitor = {
                "top": mon["top"] + pad_top,  # 100px from the top
                "left": mon["left"] + pad_left,  # 100px from the left
                "width": 1920-2*pad_left,
                "height": 1080-2*pad_top,
                "mon": monitor_number,
            }

        while "Screen capturing":
            last_time = time.time()

            # Get raw pixels from the screen, save it to a Numpy array
            img = numpy.array(sct.grab(monitor))
            results = model(img)

            # Display the picture
            # 
            results.render()
            cv2.imshow("OpenCV/Numpy normal", results.imgs[0])
            #out.write(results.imgs[0])
            #results.

            # Display the picture in grayscale
            # cv2.imshow('OpenCV/Numpy grayscale',
            #            cv2.cvtColor(img, cv2.COLOR_BGRA2GRAY))

            #results = model(img)
            t = 1 / (time.time() - last_time)
            print("\nfps: {}".format(t))
            frametimes.append(t)
            print(f"{sum(frametimes)/len(frametimes)}")
            #cv2.imshow("OpenCV/Numpy normal", img)

            # Press "q" to quit
            if cv2.waitKey(25) & 0xFF == ord("q"):
                cv2.destroyAllWindows()
                break
            
        #out.release()

